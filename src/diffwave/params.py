# Copyright 2020 LMNT, Inc. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

import numpy as np
from utils import tanh61_beta_schedule

class AttrDict(dict):
  def __init__(self, *args, **kwargs):
      super(AttrDict, self).__init__(*args, **kwargs)
      self.__dict__ = self

  def override(self, attrs):
    if isinstance(attrs, dict):
      self.__dict__.update(**attrs)
    elif isinstance(attrs, (list, tuple, set)):
      for attr in attrs:
        self.override(attr)
    elif attrs is not None:
      raise NotImplementedError
    return self


params = AttrDict(
    # Training params
    batch_size=64,
    learning_rate=1e-5,
    max_grad_norm=None,

    # Data params
    # these are actually not used for trajectories
    sample_rate=2000,
    n_mels=80,
    n_fft=1024,
    hop_samples=256,
    crop_mel_frames=62,  # Probably an error in paper.

    # Model params
    residual_layers=7,
    residual_channels=32,
    dilation_cycle_length=21, # with cycle=1 and residual layers = 8, we get r=511*T
    unconditional = True,
    #noise_schedule=np.linspace(1e-4, 0.01, 200).tolist(), # last param is num_timesteps
    noise_schedule = tanh61_beta_schedule(200).tolist(),
    inference_noise_schedule=[1e-4, 0.001, 0.01, 0.05, 0.2, 0.5,  0.6, 0.7, 0.8, 0.9], # for fast sampling
    audio_len = 2000, # length of generated samples
    checkpoints_hop = 100000 # how often to save checkpoints
)


# This is the original params dictionary

""" params = AttrDict(
    # Training params
    batch_size=16,
    learning_rate=2e-4,
    max_grad_norm=None,

    # Data params
    sample_rate=22050,
    n_mels=80, # this is actually not used for unconditional generation
    n_fft=1024,
    hop_samples=256,
    crop_mel_frames=62,  # Probably an error in paper.

    # Model params
    residual_layers=30,
    residual_channels=64, # 64 or 128 for a larger model (diffwave large)
    dilation_cycle_length=10,
    unconditional = True,
    noise_schedule=np.linspace(1e-4, 0.05, 50).tolist(),
    inference_noise_schedule=[0.0001, 0.001, 0.01, 0.05, 0.2, 0.5], 

    # unconditional sample len
    audio_len = 22050*5, # unconditional_synthesis_samples
) """
