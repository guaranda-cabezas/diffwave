# DiffWave
**Note**  
This code is an adaptation of the original work by the LMNT team. [Original repo link](https://img.shields.io/github/license/lmnt-com/diffwave); version 0.17.   

**Part of the original README follows here:**    
  

DiffWave is a fast, high-quality neural vocoder and waveform synthesizer. It starts with Gaussian noise and converts it into speech via iterative refinement. The speech can be controlled by providing a conditioning signal (e.g. log-scaled Mel spectrogram). The model and architecture details are described in [DiffWave: A Versatile Diffusion Model for Audio Synthesis](https://arxiv.org/pdf/2009.09761.pdf).

## Pretrained models
[22.05 kHz pretrained model](https://lmnt.com/assets/diffwave/diffwave-ljspeech-22kHz-1000578.pt) (31 MB, SHA256: `d415d2117bb0bba3999afabdd67ed11d9e43400af26193a451d112e2560821a8`)

This pre-trained model is able to synthesize speech with a real-time factor of 0.87 (smaller is faster).

### Pre-trained model details
- trained on 4x 1080Ti
- default parameters
- single precision floating point (FP32)
- trained on LJSpeech dataset excluding LJ001&ast; and LJ002&ast;
- trained for 1000578 steps (1273 epochs)

## Getting started
Clone the repo locally:
```
git clone https://gitlab.lisn.upsaclay.fr/guaranda-cabezas/diffwave
cd diffwave
```

### Training with audio
Before you start training, you'll need to prepare a training dataset. The dataset can have any directory structure as long as the contained .wav files are 16-bit mono (e.g. [LJSpeech](https://keithito.com/LJ-Speech-Dataset/), [VCTK](https://pytorch.org/audio/_modules/torchaudio/datasets/vctk.html)). By default, this implementation assumes a sample rate of 22.05 kHz. If you need to change this value, edit [params.py](https://github.com/lmnt-com/diffwave/blob/master/src/diffwave/params.py).

```
python ./src/diffwave/preprocess.py /path/to/dir/containing/wavs
python ./src/diffwave/__main_.py /path/to/model/dir /path/to/dir/containing/wavs

# in another shell to monitor training progress:
tensorboard --logdir /path/to/model/dir --bind_all
```

You should expect to hear intelligible (but noisy) speech by ~8k steps (~1.5h on a 2080 Ti).

**Note:** note that I've provided the full path to the python scripts; in the original repo of DiffWave, there is a guide for installing it as a package locally, so you can instead call it as a module, i.e.: ```python -m diffwave.preprocess ... ```. Check the original repo if you want to use these commands instead. However, because I have customized the code for training with other type of data (possibly changing code every now and then), I preferred to avoid the installation of diffwave.

### Training with trajectories
Training is currently possible for one dimensional trajectories only. Any preprocessing done to the trajectories (e.g. normalization and scaling) is not parameterized in the code. This is, you have to manually code it and pass the pertinent transformations to the ```ParticleDatasetVx``` class in ```src/diffwave/dataset.py```. There are classes for both standard normalization and scaling in ```src/data/lagrangian_datatools.py```.

Example command to train a model: 
```
python src/diffwave/__main__.py /path/to/model/dir /path/to/file/containing/trajectories --data_type trajectories_x # for 1D data
```

#### Multi-GPU training
By default, this implementation uses as many GPUs in parallel as returned by [`torch.cuda.device_count()`](https://pytorch.org/docs/stable/cuda.html#torch.cuda.device_count). You can specify which GPUs to use by setting the [`CUDA_DEVICES_AVAILABLE`](https://developer.nvidia.com/blog/cuda-pro-tip-control-gpu-visibility-cuda_visible_devices/) environment variable before running the training module.

### Inference
Inference of trajectories:
```
python ./src/diffwave/inference.py ./models/weigths.pt -o ./path_to_file/that/stores/new_samples.npy \
--data_type trajectories_x --cpu --num_samples 100 --clamp False
```
Inference of audio:
```
python ./src/diffwave/inference.py --fast /path/to/model /path/to/spectrogram -o output.wav
```

## References
- [DiffWave: A Versatile Diffusion Model for Audio Synthesis](https://arxiv.org/pdf/2009.09761.pdf)
- [Denoising Diffusion Probabilistic Models](https://arxiv.org/pdf/2006.11239.pdf)
- [Code for Denoising Diffusion Probabilistic Models](https://github.com/hojonathanho/diffusion)
